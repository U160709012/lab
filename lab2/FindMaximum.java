public class FindMaximum {

	public static void main(String[] args){
		int value1 = 3;
        int value2 = 5;
        int result;

        boolean someCondition = value1 > value2;

        result = someCondition ? value1 : value2;

        System.out.println(result);

	}
}
